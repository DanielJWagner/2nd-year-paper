
FAOstat has recurrent government spending on agriculture but it is not broken down further
and there appears to be a lot of missing data
http://www.fao.org/faostat/en/#data/IG
Can find government expenditure in the data explorer under investment
Also has some statistics on food security

US FAS PSD all data has 65 crops, though one or 2 are quite narrow, ie local soybean meal
tobacco does not appear to be included

UN COMTRADE- imports are cif(cost insurance and freight) while exports are FOB
as much as 20% difference; imports usually recorded more accurately

WITS has data on trade composition https://wits.worldbank.org/
may be relevant

OECD has significantly fewer crops but there is some overlap at least for some countries
specifically maize, rice, and refined sugar
OECD methodology: http://www.oecd.org/tad/agricultural-policies/psemanual.htm
Commodities below 1% of the value of agricultural production do not have support measures
calculated
Price supports are already included when Producer Support Estimate is calculated,
also includes all aggregate budgetary transfers- this category does not seem to
include taxes as 0s are reported in many years
there is no specific mention of 0s but because this includes prices supports
government intervention to reduce prices could count negatively against the total
It is also possible that there are penalties to discourage certain crops


1 estimate per market year, multiple per calendar year
it seems calendar years may be when the estimates were produced?  Calendar years 
mostly seem to be batches, 1-2 years per country/crop combination while market years
reflect a much longer timeline

using market year dramatically changed results, only significance with commodity breakdown
need to include heterogenous effects with 

OECD producer estimates has & of value breakdown for subsidies by crop
1986-2017

WTO may have AMS breakdown but it's buried

Hong Kong Declaration in 2005 was to end export subsidies in Advanced Markets
by 2013


Lopez sugar paper 1989

Sugar support mainly through quotas and price support, greater of supported price
or the world price

Have methods for producer and consumer surplus- demand equation that takes substitues
into account; for PS deflate using cost of inputs- not sure if I have data on this

IZEF- iterative zellner's seemingly unrelated procedure- replacement for MLE?

Government publications have acreage data for sugar

Little connection between producer surplus and price targets?!
Sugar market in US seems to be highly distorted



Beghin et Al, Cost of US Sugar program revisited
significant deadweight loss both from quota tariff and forfeiture of sugar to guarantee prices
Program no longer revenue neutral as of 2001, accounts to subsidy = 50% of value in paper
find source for this or if WTO/someone else has calculated subsidy index
look up Heart & Babcock 2001
$500 mil in deadweight loss in 1999/2000
US welfare losses approaching $1 bil in 1998
elimination will benefit consumers, may hurt food producers
estimate with multimarket model under different passthrough assumptions


Implications of the WTO on the Redesign of U.S.
Farm Policy
Heart & Babcock 2001- seems like a dead end at least for my purposes
about modification of US subsidies to meet Uruguay round standards but is old
and does not prominently feature the sugur subsidy estimation referenced in Beghin
has AMS support for 24 crops 1996-2002, will just use OECD data instead of this
since it is more complete and no source is listed for the figures


Demidova & Rodriguez-Clare 2013, Melitz model in small economy

Without outside good unilateral reduction of trade barriers increases welfare as
price index declines more than wages since the price index is not staked to a
homogenous good
Applicable to this paper?  Goods are homogenous but still traded, so not country
specific


Davis & Harrigan 2011
Workers lose rents from job churn, could combine with GAEZ data to show welfare loss?  May be impossible to measure
empirically but still worth mentioning


Boysen, Jensen, and Matthews 2016
EU Common Agricultural Policy(CAP) increases poverity in Uganda
May benefit net food importers
Uganda has tariff free access as of 2007, Contonou Agreement & everything but arms
suffers some from preference erosion due to tariff relaxtion on others
but benefits from cuts in payments as crop prices rise, shifts to more exporting
of commodities, outweighs negative tariff effect
rural households benefit more- assumes rather high degree of production coupling

No regressions performed for this study; simply filled out databases for simulations
and proceeded from there- read details of model to understand their assumptions?



Boysen 2016- Food Demand Characteristics in Uganda
estimates Ugandan demand elasticities in relation to price, subsidies, and urban vs rural
Regression is OLS to estimate price(unit value?) on household assets per capita,
household characteristics, cluster dummies,

this is integrated with a budgeting demand system- need to read this paper thoroughly
may be too micro focused but hopefully has some ideas?

Uganda has enough food to meet needs but significant fraction of population is not
receiving recommended daily allotment of calories

percent expenditure on food = intercept +household characteristics + price index + quadratic per capita expenditure term

can get expenditure data nationally w/ per capita income & savings rate

uncompensated elasticity = -1 + (price index coefficient/% expenditure on food)

compensated(hicksian) price elasticity = uncompensated price elasticity + (expenditure)(% food expenditure)

Look into QUAIDS, Bank et al 1997, and AIDS Deaton & Mille 1980(Almost ideal demand system)

2nd stage breaks out this planned price expenditure among various food groups using the form

% expenditure on group i = intercept + log price of substitute + log(food PCE/translog price index) + ((previous term)^2)/b(q)
where b(q) is a cobb douglas price aggregator

translog price index:
ln a(p) = intercept + sum[price of good j] + 1/2(sum(sum(log price of j * log price of l))

Cobb douglas price aggregator:
b(p) = price of j^beta j multiplied together for all j in the set of goods
with beta being the linear cross price elasticity

could have 2 step determination of whether to consume crop in question or
subsidize?
possibly a 2 stage regression, first to estimate subsidy effect on price
and then price effect on welfare/consumption



Pick and Park 1991
US does not price discriminate in cotton, corn, & soybeans
USSR and China were able to depress their wheat price with market power
References some international oligopoly models
IMF exchange rate data http://data.imf.org/?sk=4C514D48-B6BA-49ED-8AB9-52B0C1A0179B
subsitution between feed types is common (corn vs soybeans etc)
soybeans also not an oligopoly for this reason

Deodhar & Sheldon 1997
Soymeal and soyoil are independent markets
Major producers of soymeal are Argentina, Brazil, US, & EU
Brazil & Argentina offer tax subsidies to encourage domestic processing
of soybeans; fishmeal is major competitor with soybean meal
US ag has publications on prices
soymeal is perfectly competitive market

Torayeh 2011
Estimates developing country export share elasticity with respect to aggregate
of US & EU amber box subsidies
uses this to simulate effect of lowered/removed subsidies
difference in stated and actual support, not clear what this is based on
estimates reductions based on Hong Kong declaration 2004



EU Cap Analysis 2014-2020
Nominal Protection Coefficient- is there a list of these available?
OECD implies NPC is in their data
No empirical details- this is just a literature review?
Boysen is the only author to have looked at EU CAP impact on 
a developing country- opportunity to extend the literature


Helming & Tabeau 2017- may be useful to look at the simulation models used here
otherwise relevance is not clear

CAPRI funded by EU- partial equilibrium model of EU agriculture
https://www.capri-model.org/dokuwiki/doku.php
GAMS dependency is $640 for unlimited version, and needs CONOPT solver
Additional $640 for GAMS with CONOPT
there is a limited free GAMS- look into this
no download link for CONOPT- maybe talk to Heins about this?
it seems to be integrated with other packages
MAGNET appears to be run by a private institute- global GE model
http://www.magnet-model.org/
license for software dependency GEMPACK is several hundred dollars
can get limited version for free as educational institution
can get student intel version https://software.intel.com/en-us/qualify-for-free-software
, but limited GEMPACK is probably insufficient
Need GEMPACK to modify base GTAP model, which is free if theoretical base is not modified


Helming is an author of MAGNET- does this mean you have to be an author to use a model?
Switching EU subsidies to employee rather than decoupled or hectare based increased
EU agricultural production and depressed prices,
lowered land value in countries where subsidy rent was factored in
production increase in labor intensive industries



Jensen & Miller 2011
Little evidence that food subsidies improve nutrition among the poor
consumers substitute based on taste or to non-food purchases
decrease in calories when already below recommended level,
some increase in vitamins, little is statistically significant

Similar evidence from papers on price, summarized in 
Behrman and Deolalikar 1988, Nutrients: Impacts and Determinants

Nutrition should not be considered as a welfare channel though
welfare of the poor is improved through substitution to non food or
better tasting goods



Dame & Nisser 2011- Subsidies and agricultural production in North India

specifically looking at food security, malnourishment is a 
persistent issue in mountain areas
Look at Indian Public Distribution- development aid &
its affect on household production
No formal model but households subsitute to safer barley when wheat is subsidized
and available for sale; countries may switch to more reliable crops if food
diversity is provided by subsidiziation of substitute goods
consider including a cross-crop subsidy term




Cornia 1985

Small farms are more efficient due to higher use of inputs,
proposals to redistribute land to increase efficiency & meet nutritional needs
of larger segment of the population

Main takeaway- land area may lead to lower yields, also this paper does some work
on production functions that may be useful

Production function looks at land availability, capital inputs, land intensity,
and intermediate inputs to estimate a production function, summing the estimated4
exponents to determine returns to scale

What am I trying to estimate?  Not so much a production function as a decision to produce a crop?
Trying to estimate a melitz-like cutoff past which a country does not produce?
FAOstat and WDI might have variables that would help construct my function

De Castris & Di Gennaro Working 2018
Spacial regression of southern Italian farms to determine effectivness of CAP
in stimulating production, spacial augmented cobb douglas w/ micro-founded
quantile version of a spatial lag model; support is statistically significant in higher
quantiles with spillover effects

Estimate porduction function with Y as value added based on labor, fixed capital,
ground extension and subsidies- log transformed, on a per farm basis using RICA data

Spacial autocorrelation? Spatial copy of a signal with delay- repeating spillover effects?
Only very large farms benefit from more land, dimishing returns to subsidies but
with positive spillover effects

Labor focused CAP would be more effective

Look at Fader et al 2013 and Porkka et al on using imports to satisfy internal agricultural
demand; subsidies have ambigious effect on efficiency

Look at Griliches 1964 aggregated production function
This is an actual production function with labor, capital,
and inputs used- this would be a major direction change
for this paper


Moore 2004

Case study of Malian cotton producers
Cotton is most important source of export income
Joint venture CMDT owns most malian cotton and exports
it as fiber
Mali offers no subsidies, World Bank dictates no protectionist
measures, seems likely other poor countries will be subject to the same
terms

For price theory look into Lamb & Ethridge & Tyler, 
"analysis of the african cotton economy in relation to proposed
multilateral trade reforms"





Etile and Oberlander Forthcoming

Literature review of economic causes of obesity
Trade openness increases obesity but social globalisation more important
 chronic diseases have impact
on health care costs and human capital
Research on food is limited, mostly limited time periods
in wealthy countries, lack of causal evidence
Obesity tripled since 1975, spreading to lower income countries

National diet doesn't explain, Japan has low obesity but
Chile has similar seafood focus yet matches US rate
possible link to trade opening in 1973
Low & middle income countries liberalized agricultural
markets in 90s, with WTO including agriculture in rules 
in '94, Hawkes 2006 Brazil liberalized & lowered soybean oil
prices leading to spike in usage

Food imports as share of GDP more than doubled btw '74
and 2004, increasing FDI in food but that is probably
outside of the scope of the data I have

Placement contracts from foreign companies
increase share of processed foods in diet
Real price of processed food has fallen while
fresh food has risen, processed food price less volatile

Atkin 2013 looks at trade liberalisation and relative food prices
GE framework, foreign demand for local food raises its price, 
reducing consumption

Cornelsen et all look at price elasticities, higher in
low income countries, cross-price elasticities are
relatively small, but there is no standard method
among the many studies looking at this, 
not much observed of direct long term effect of
price changes

Might be able to do something with trade openness
in another paper, maybe looking at trade networks and
obesity?  Entry of fast food restaurants into new markets?

KOF globalisation indices? (Dreher, Gaston & Martens 2008)

Household production theory- opportunity cost of cooking is
roughly the wage that could be earned outside home

habit formation in consuming food, subsidised goods could 
continue to be consumed even after subsidy is tapered, or
consumption of food overall would remain high even if consumption
of imports fell off, which is then transmitted to the next generation
strong evidence of this, see the paper section 6


I'm not sure any more of my notes are relevant to my 2nd year paper,
mostly tangentially related trade things, may add them if lit review
needs to be beefed up further


USDA has direct farm payments by state
could do a state-by-state look at uncoupled payments



https://eh.net/encyclopedia/common-agricultural-policy/
CAP has a target price, needs further investigation


https://voxeu.org/article/impacts-export-taxes-agricultural-trade
paper on export taxes, reintroduced after food price volatility around 2010
imposed for food security purposes, most common type of export restriction
recently; $33 billion in welfare losses per year due to export taxes
lowers prices domestically but raises them for the world, obviously based on 
market power of country in the given crop(s)
Food importers cannot insulate themselves from price swings in this way
but export taxes may exacerbate rising world food prices

Run partial equilibrium dynamic gravity model, find effects, often in same period
also run a computable general equilibrium(GCE) model
removing export tax helps country with the tax but hurts countries competing with it
negligable effect on world prices, small poverty decrease



Burlacu 2018
Background on Romanian ascension to the EU; CAP officially came into force in 2007
when Romania joined the EU
Convergence deadlines at end of 2009 and 2013, source I can't document said subsidies
not fully phased in for 10 years?
Bulgaria joined as well in 2005 Treaty of Accession


Said Shelaby 2014

Bilateral agricultural trade in Egypt with the Gravity model
1997 Arab trade pact GAFTA, fully implemented in 2005
There is an arab monetary fund
This article has multiple grammar and spelling errors,
is it credible?  Is any work with the gravity model
credible?  maybe it is ok as a modification
methodological flaws, metrics not explained and/or calculated
over a truncated range without explanation
paper is pretty bad, suggesting "increasing GDP" to increase trade power
might take some of the background sources but otherwise cannot
cite this paper I think
only 13 countries in sample


Yu 2016
An Influence Factor Analysis of International Trade Flow using the Gravity Model

Again, typos and looks fairly bog standard except for looking at "system arrangement?"
Only 30 countries in the sample, focus on agriculture with arable land and exchange
rates as variables
Can use this to hunt through its model and sources but I don't think the paper 
is citeable, doesn't really stand up to scrutiny
Fisher ADF and Fisher PP?




Gravity Model Notes

Maybe do a modified form of the gravity model focusing on domestic production?
try not just copying an existing trade model?
Gravity as a simple baseline to evaluate the effects of a changing
subsidy regime
maybe look at if subsidies supress trade?
Not worrying so much about what drives trade as if subsidies
affect the existing patterns once they are controlled for


Gravity model is also used to study size/travel between cities?

Relevant Trade Pacts:
These are all likely endogenous on distance & culture

East African Commmunity, formed 2000, Expanded 2007

Association of Southeast Asian Nations
Formed 1967- Indonesia, Malaysia, Philippines, Singapore, Thailand
added Brunei 1984, Vietnam in 1995, Laos and Myanmar in 1997
Cambodia joined in 1999
ASEAN +3 in 1997- China, Japan, and South Korea for economic cooperation
Phasing out of tariffs beginning in 1992 
AFTA- ASEAN Free Trade Area- 1992 for ASEAN members
2015 Economic community, slow rollout if not delayed
2009- FTA with Australia and New Zealand

CARICOM- Carribean Single Market and Economy (CSME)
Formed in 1990, not implemented until 2006, phase 1 in 2008
Original Members- Antigua & Barbuda, Barbados, Belize, Dominica, Grenada,
Guyana, Jamaica, Saint Kitts & Nevis, Saint Lucia, Saint Vincent & Grenadines
Trinidad & Tobago
Suriname added in 1995, Haiti in 2002


Mercosur- Southern common Market
Established 1991 & 1994
Argentina, Brazil, Paraguay, Uruguay
Venezuela suspended at end of 2016



WTO agricultural support definitions

de minimis- minimal amounts of domestic support that are allowed even though they
distort trade- 5% of production value for developed countries, 10% for developing
https://www.wto.org/english/thewto_e/glossary_e/de_minimis_e.htm

Amber box- all subsidies not in blue or green box, generally distorting
subject to de minimis restrictions

https://www.wto.org/english/tratop_e/agric_e/agboxes_e.htm

Blue box- Amber box with conditions; amber subsidies that require farmers to
limit production; currently no cap on blue box subsidies

Green box subsidies- do not distort trade, general not targeted at specific products
decoupled from production levels, no limit on amount, some argue not all support in
this box is non-distorting due to size of support


Hamilton critique of original paper
Endogeneity of subsidy decisions- look at the literature on this
sugar paper seemed to suggest there was not a clear mechanism
No one is able to influence subsidies entirely? Budget considerations
and other forms of support that are not easily trackable- using sugar as loan collateral

